benchmark:
	go test -bench=. -benchmem

profile:
	go test -bench=. -benchmem -cpuprofile cpu.out -memprofile mem.out

cpu_prof_viz:
	$(MAKE) profile
	go tool pprof -http localhost:8093 --nodefraction=0.1 fast-response.test cpu.out

mem_prof_viz:
	$(MAKE) profile
	go tool pprof -http localhost:8093 --nodefraction=0.1 fast-response.test mem.out
