package main

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"	
)

var payload = `{
"kind": "event",
"val": 2423534,
"meta": {
  "key": "test_key",
  "value": "lorem ipsum fuck you!",
  "option": true
}
}`

func BenchmarkCreateMessage(b *testing.B) {	
	for n := 0; n < b.N; n++ {
		req := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(payload))
		w := httptest.NewRecorder()

		CreateMessage(req, w)
	}
}

func BenchmarkCreateMessagePool(b *testing.B) {	
	for n := 0; n < b.N; n++ {
		req := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(payload))
		w := httptest.NewRecorder()

		CreateMessagePool(req, w)
	}
}
