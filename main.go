package main

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"sync"
	"time"
	
	"github.com/golang/glog"
//	"github.com/valyala/fasthttp"
	json "github.com/json-iterator/go"
)

type Message struct {
	Kind string `json:"kind"`
	Val  int64  `json:"val"`

	Meta struct {
		Key    string `json:"key"`
		Value  string `json:"value"`
		Option bool   `json:"option"`
	}
}

func CreateMessage(r *http.Request, w http.ResponseWriter) {
	go processMessage(r.Body)
	fmt.Fprint(w, "ok")
}

func processMessage(body io.Reader) {
	msg := &Message{}
	
	if err := json.NewDecoder(body).Decode(&msg); err != nil {
		glog.Errorf("failed to decode payload: %v", err)
	}

	time.Sleep(20 * time.Millisecond)
}

var bufferPool = sync.Pool{
	New: func() interface{} {
		return new(bytes.Buffer)
	},
}

func CreateMessagePool(r *http.Request, w http.ResponseWriter) {
	body := bufferPool.Get().(*bytes.Buffer)

	if _, err := io.Copy(body, r.Body); err != nil {
		glog.Errorf("failed to copy payload: %v", err)
	}
	
	go processMessagePool(body, func(){bufferPool.Put(body)})
	fmt.Fprint(w, "ok")
}

func processMessagePool(body *bytes.Buffer, callback func()) {
	msg := &Message{}
	
	if err := json.NewDecoder(body).Decode(&msg); err != nil {
		glog.Errorf("failed to decode payload: %v", err)
	}
	callback()

	time.Sleep(20 * time.Millisecond)
}

// func CreateMessagePoolFasthttp(ctx *fasthttp.RequestCtx) {
// 	body := bufferPool.Get().(*bytes.Buffer)

// 	if _, err := io.Copy(body, r.Body); err != nil {
// 		glog.Errorf("failed to copy payload: %v", err)
// 	}
	
// 	go processMessagePool(body, func(){bufferPool.Put(body)})
// 	fmt.Fprint(w, "ok")
// }

	

func main() {

}
